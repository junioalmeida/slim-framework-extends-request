# Clase que extende Request de Slim Framework 3

Essa classe foi criada para extender a classe original \Slim\Http\Request e sobrescrever o método chamado getParam.
A sobrescrita apenas agrega uma validação e em seguida chama o getParam() original repassando os mesmo parametros originais.
Vale lembrar que essa função recebe parametros de GET, POST e também do BODY quando é enviado um JSON.

#### Validação agregada:
- Se o usuário deixar de enviar um parametro, e não tiver seteado um default: uma Exceção é lancada. 
- Se é passado um valor default, nada é alterado no seu funcionamento padrão.


## Instalação

**1 -** Adicionar nas dependencias este código:
<!-- language: PHP -->
    // Configuração da nova classe Request para atribuir a validação
    $container['request'] = function ($c) use ($app) {
        $Request = \JunioDeAlmeida\Slim\Request::createFromEnvironment($c['environment']);
        $Request->setApp($app);
        return $Request;
    };
    
    
**2 -** Confira no arquivo `/public/index.php` se o nome da variavel que recebe a instancia da aplicação \Slim\App chama `$app` :
<!-- language: PHP -->
    session_start();
    
    // Instantiate the app
    $settings = require __DIR__ . '/../src/settings.php';
    $app = new \Slim\App($settings);
    
    // Set up dependencies
    require __DIR__ . '/../src/dependencies.php';
Caso não seja, adapte este trecho do código: `function ($c) use ($app) {` em:
<!-- language: PHP -->
    $container['request'] = function ($c) use ($app) {
    
    
**2 -** Se julgar necessário, configure o arquivo settings com os seguintes possíveis parametros:
<!-- language: PHP -->
    return [
        'settings' => [
    
            // ... here another settings
    
            // Parametros para customizar a validação de parametros obrigatórios
            'param_required' => [
                'exception_class' => '\Exception', // Classe de exception que será lancáda
                'exception_messege' => 'The "%s" parameter is required!', // Mensagem que será usada no lançamento
            ]
        ],
    ];
